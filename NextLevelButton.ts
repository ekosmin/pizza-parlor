///<reference path="phaser/typescript/phaser.d.ts"/>
///<reference path="Level.ts"/>
///<reference path="LevelSettings.ts"/>
///<reference path="Storage.ts"/>
module Main {

    export class NextLevelButton extends Phaser.Sprite {

        private level: Level;

        /**
         * Constructs a next level button in the lower-left corner of the screen.
         * @param level
         */
        constructor(level:Level) {
            super(level.game, 0, 0, 'nextButton');
            this.level = level;

            this.x = 0;
            this.y = this.level.game.height - this.height;

            this.inputEnabled = true;
            this.events.onInputDown.add(this.nextLevel, this);
        }

        private nextLevel(): void {
            LevelSettings.getInstance().setLevel(LevelSettings.getInstance().getLevelNumber() + 1);
            Storage.setNumber(LevelSettings.LEVEL_KEY, LevelSettings.getInstance().getLevelNumber());
            this.game.state.start('Level', true, false, this.level.chomp);
            this.level.nextLevelButton = null;
        }

    }
}