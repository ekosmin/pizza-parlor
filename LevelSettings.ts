///<reference path="phaser/typescript/phaser.d.ts"/>
///<reference path="Fraction.ts"/>
module Main {

    export class LevelSettings {

        public static LEVEL_KEY: string = 'level';

        private static instance: LevelSettings = null;

        private basePizzaAmount: Fraction;
        private multipliers: Fraction[];
        private heads: number[];
        private isServed: boolean[];
        private levelNumber: number = -1; // so the first level is 0

        constructor() {
            if (LevelSettings.instance) {
                throw new Error("Error: instantiation failed. Use LevelSettings.nextLevel() to obtain the next level");
            }
            LevelSettings.instance = this;
        }

        public getBasePizzaAmount(): Fraction {
            return this.basePizzaAmount;
        }

        public getMultipliers(): Fraction[] {
            return this.multipliers;
        }

        public getHeads(): number[] {
            return this.heads;
        }

        public getServed(): boolean[] {
            return this.isServed;
        }

        public getLevelNumber(): number {
            return this.levelNumber;
        }

        public isInitialized(): boolean {
            return this.levelNumber != -1;
        }

        public static getInstance(): LevelSettings {
            if (LevelSettings.instance === null) {
                LevelSettings.instance = new LevelSettings();
            }
            return LevelSettings.instance;
        }

        public setLevel(levelNumber: number): void {
            this.levelNumber = levelNumber;
            switch (levelNumber) {
                case 0:
                    this.updateLevel(new Fraction(1, 4),
                        [new Fraction(1, 2)],
                        [4, 4, 2],
                        [true, false, false]);
                    break;
                case 1:
                default:
                    this.updateLevel(new Fraction(1, 2),
                        [new Fraction(3,2)],
                        [2, 3],
                        [false, true]);
                    break;
            }
        }

        private updateLevel(basePizzaAmount: Fraction, multipliers: Fraction[], heads: number[],
                             isServed: boolean[]): void {
            this.basePizzaAmount = basePizzaAmount;
            this.multipliers = multipliers;
            this.heads = heads;
            this.isServed = isServed;
        }

    }

}