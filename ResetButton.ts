///<reference path="phaser/typescript/phaser.d.ts"/>
///<reference path="Level.ts"/>
///<reference path="LevelSettings.ts"/>
///<reference path="Storage.ts"/>
module Main {

    export class ResetButton extends Phaser.Sprite {

        private level: Level;

        /**
         * Constructs a reset button in the lower-right corner of the game.
         */
        constructor(level:Level) {
            super(level.game, 0, 0, 'resetButton');
            this.level = level;

            this.x = this.level.game.width - this.width;
            this.y = this.level.game.height - this.height;

            this.inputEnabled = true;
            this.events.onInputDown.add(this.resetProgress, this);
        }

        private resetProgress(): void {
            LevelSettings.getInstance().setLevel(0); // Must set here in case storage is not available
            Storage.setNumber(LevelSettings.LEVEL_KEY, 0);
            this.game.state.start('Level', true, false, this.level.chomp);
        }

    }
}