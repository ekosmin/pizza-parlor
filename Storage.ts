///<reference path="chrome.d.ts"/>

module Main {

    export class Storage {

        private static result: string;
        /**
         * Sets the key to the given value if storage is available; else does nothing
         */
        public static setString(key: string, value: string): void {
            if (chrome.storage) {
                console.log("storing " + key + ": " + value);
                var dict = {};
                dict[key] = value;
                chrome.storage.sync.set(dict);
            }
        }

        public static setNumber(key: string, value: number): void {
            Storage.setString(key, "" + value);
        }

        /**
         * Looks up the value associated with the given key, parses it as an int, and applies it to the given callback
         * @param key The key to lookup. If it is not present in storage, the value applied is NaN
         * @param callback The callback function to which the value will be applied
         * @param thisArg The "this" argument to be used in the callback function
         * @param defaultValue The value applied to callback if none is found in storage
         */
        public static getNumber(key: string, callback: Function, thisArg: any, defaultValue?: number): void {
            if (defaultValue == undefined) {
                defaultValue = NaN;
            }

            if (chrome.storage) {
                chrome.storage.sync.get(key, function(result) {
                    var value: number = parseInt(result[key]);
                    console.log("value = " + value);
                    if (isNaN(value)) {
                        value = defaultValue;
                    }
                    callback.apply(thisArg, [value])});
            } else {
                callback.apply(thisArg, [defaultValue]);
            }
        }

        /**
         * Looks up the value associated with the given key, and applies it to the given callback
         * @param key The key to lookup. If it is not present in storage, the value applied is null
         * @param callback The callback function to which the value will be applied
         * @param thisArg The "this" argument to be used in the callback function
         * @param defaultValue The value applied to callback if none is found in storage
         */
        public static getString(key: string, callback: Function, thisArg: any, defaultValue?: string): void {
            if (defaultValue == undefined) {
                defaultValue = null;
            }

            if (chrome.storage) {
                chrome.storage.sync.get(key, function(result) {
                    var value: string = result[key];
                    if (value == null) {
                        value = defaultValue;
                    }
                    callback.apply(thisArg, [value])});
            } else {
                callback.apply(thisArg, [defaultValue]);
            }
        }
    }
}